// [Section] MongoDB aggregation
  /*
    - to generate and perform operations to create filtered results that helps us analyze data
  */

  /*
    using aggregate method:
      -the "$match" is used to pass the documents that meet the specified condition to the next stage or aggregation process
      Syntax:
        db.collectionName.aggregate([{$match:{field: value}}])
  */

  db.fruits.aggregate([{$match: {onSale: true}}])

  /*
    - The $group is used to group elements together and field-value the data from the grouped element
    Syntax:
      {$group: _id: "$fieldSetGroup"}
  */

  db.fruits.aggregate([
    {
      $match: {onSale: true}
    },
    {
      $group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}
    }
    ])

  // Mini activity:
  // First get all the fruits taht are color yello then grouped them into their respective supplier and their available stock.

  db.fruits.aggregate([
    {
      $match: {color: "Yellow"}
    },
    {
      $group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}
    }
    ])

  // Field Projection with Aggregation
    /*
        -$project can be used when aggregating data to include/exclude from the returned result
        Syntax:
          {$project: {field: 1/0}}
    */

  db.fruits.aggregate([
    {
      $match: {onSale: true}
    },
    {
      $group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}
    },
    {
      $project: {_id:0}
    }
    ])

  // Sorting Aggregated Results
    /*
      - $sort can be used to change the order of the aggregated result
    */
  db.fruits.aggregate([
    {
      $match: {onSale: true}
    },
    {
      $group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}
    },
    {
      $sort: {totalFruits: 1}
    }
    ])
  /*
    the value in sort:
    1 - lowest to highest
    -1 - highest to lowest
  */

  // aggregating results based on an array fields
    /*
      - the $unwind deconstructs an array field from a collection/field with an array value to output result
    */

  db.fruits.aggregate([
  {
    $unwind: "$origin"
  },
  {
    $group: {_id: "$origin", fruits: {$sum: 1}}
  }
  ])

  // [section] Other aggregate stages
  // $count all yellow fruits

    db.fruits.aggregate([
    {
      $match: {color: "Yellow"}
    },
    {
      $count: "Yellow Fruits"
    }
    ])

  // $avg gets the average value of the stock

    db.fruits.aggregate([
    {
      $match: {color: "Yellow"}
    },
    {
      $group: {_id: "$color", avgYellow: {$avg: "$stock"}}
    }
    ])

  // $min && $max

    db.fruits.aggregate([
    {
      $match: {color: "Yellow"}
    },
    {
      $group: {_id: "$color", lowerStack: {$min: "$stock"}}
    }
    ])

    db.fruits.aggregate([
    {
      $match: {color: "Yellow"}
    },
    {
      $group: {_id: "$color", highestStack: {$max: "$stock"}}
    }
    ])